/* @JUDGE_ID: 12085 C++  "Mobile Casanova" */

/**************
by: Carlos Mera
date: 16/10/2017

**************/
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;



int main() {
  // Numeros de conjuntos de telefonos
  int N;
  cin >> N;

  unsigned int test = 1;
  while (N) {
    vector<unsigned long int> phones;
    for (int i = 0; i < N; i++) {
      unsigned long int temp;
      cin >> temp;

      phones.push_back(temp);
    }

    // Eliminar este ordenamiento
    sort(phones.begin(), phones.end());

    vector<unsigned long int>::iterator it = phones.begin();
    cout << "Case " << test << ":" << endl;
    while (it < phones.end()) {
      unsigned int flag = 0;
      vector<unsigned long int>::iterator next = it+1;
      // Hacer primero la condicion de *it + 1 == *next y despues la magia
      while (next < phones.end()) {
        if (*it + flag + 1 == *next) {
          flag++;
          next++;
        } else {
          break;
        }
      }
      if ( !flag ) {
        cout << "0" << *it << endl;
        it++;
      } else {
        // Usar modulo potencia de 10
        int start_r = *it, end_r = *(it + flag);
        string start = to_string(start_r);
        string end = to_string(end_r);
        int diff;
        for (int i = 0; i < start.length(); i++) {
          if (start[i] != end[i]) {
            diff = i;
            break;
          }
        }
        string final(end.begin()+diff, end.end());
        cout << "0" << *it << "-" << final << endl;
        it+=flag+1;
      }
    }
    test++;
    cout << endl;
    cin >> N;
  }

  return 0;
}
