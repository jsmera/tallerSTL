/* @JUDGE_ID: 11321 C++  "Sort! Sort!! And Sort!!!" */

/**************
by: Carlos Mera
date: 21/10/2017

**************/
#include <iostream>
#include <algorithm>
#include <vector>



using namespace std;

struct Numbers {
  long int n;
  int pos;
};

bool isPar(long int n) {
  return !(n%2);
}

bool comp(const Numbers& x, const Numbers& y) {
  if (x.pos != y.pos)
    return x.pos < y.pos;

  if (isPar(x.n) && isPar(y.n))
    return x.n < y.n;

  if (!isPar(x.n) && !isPar(y.n))
    return x.n > y.n;

  return !isPar(x.n);
}

int main() {
  int N, M;
  cin >> N;

  while (N) {
    cin >> M;
    long int number;

    vector<Numbers> array(10001);
    for (int i = 0; i < N; i++) {
      cin >> number;
      array[i].n = number;
      array[i].pos = number%M;
    }

    sort(array.begin(), array.begin()+N, comp);
    
    cout << N << " " << M << endl;
    for (vector<Numbers>::iterator it = array.begin(); it != array.begin()+N; it++)
      cout << it->n << endl;

    cin >> N;
  }
  cout << "0 0" << endl;

  return 0;
}