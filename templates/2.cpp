/* @JUDGE_ID: 10919 C++  "Prerequisites?" */

/**************
by: Carlos Mera
date: 16/10/2017

**************/
# include <iostream>
# include <vector>
# include <algorithm>

using namespace std;



class Category {
  public:
    vector<int> courses;
    int minimun;
    Category();
};

Category::Category() {
}

int main() {
  while (true) {
    // k: Numero de curso tomados. m: numero de categorias
    int k, m;
    cin >> k;
    cin >> m;
    
    // Si se escribe un cero la pruba acaba
    if (!k) {
      break;
    }

    // Lleno el vector con mis cursos
    vector<int> my_courses;
    for (int i = 0; i < k; i++) {
      unsigned int id;
      cin >> id;
      my_courses.push_back(id);
    }

    // Lleno el vector de las categorias
    vector<Category> categories;
    for (int i = 0; i < m; i++) {
      // c: Numero de cursos en la categoria. r: Numero de cursos minimos a tomar
      int c, r;
      cin >> c;
      cin >> r;

      Category c1;
      c1.minimun = r;
      for (int i = 0; i < c; i++) {
        unsigned int id;
        cin >> id;
        c1.courses.push_back(id);
      }
      categories.push_back(c1);
    }

    // Solucion
    // Recorro el vector de categorias
    bool result = true;
    for (vector<Category>::iterator it = categories.begin(); it != categories.end(); it++) {
      int finding = 0;
      // Recorro los cursos tomados
      for (vector<int>::iterator item = my_courses.begin(); item != my_courses.end(); item++) {
        vector<int>::iterator p;
        // Busco uno de los cursos tomados en la categoria
        p = find(it->courses.begin(), it->courses.end(), *item);
        // Si el curso corresponde a la categoria se suma uno a los cursos encontrados
        // por categorias
        if (p != it->courses.end()) {
          finding++;
        }
      }

      // Si la cantidad de cursos tomados que esten dentro de la categoria es menor a su minimo requerido
      // El estudiante no pasa
      if (finding < it->minimun) {
        result = false;
        break;
      }
    }
    result ? cout << "yes" : cout << "no"; 
    cout << endl;
  }
  return 0;
}
