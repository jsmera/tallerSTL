#include <iostream>
#include <cmath>
#include <math.h>
#include <string>
#include <cstdio>
//#define pi acos(-1.0)
using namespace std;
int main() {
	int cases;
	int moves;
	cin >> cases;
	int i = 0;
	string direction;
	const double pi = acos(-1);
	while (i < cases) {
		double x = 0;
		double y = 0;
		cin >> moves;
		int z = 0;
		//angulo
		double gr=0;
		double units;
		while (z < moves) {
			cin >> direction >> units;
			
			if(direction[0] == 'f'){
                x += units*cos(gr*pi/180);
				y += units*sin(gr*pi/180);
			}
            else if(direction[0] == 'b'){
                x -= units*cos(gr*pi/180);
				y -= units*sin(gr*pi/180);
			}
            else if(direction[0] == 'l') {
                gr += units;
                gr = fmod (gr, 360);
            } else {
                gr -= units;
                gr = fmod (gr, 360);
            }
			z++;
		}
		double value=sqrt(x*x+y*y);
		printf("%.lf\n",value);

		i++;

	}

	return 0;
}
