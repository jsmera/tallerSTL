/* @JUDGE_ID: 612 C++  "DNA Sorting" */

/**************
by: Carlos Mera
date: 17/10/2017

**************/
#include <iostream>
#include <map>
#include <vector>
#include <string>
#include <algorithm>

using namespace std;



bool comp (const pair<string, int>& x, const pair<string, int>& y) {
  return x.second < y.second;
}

int main() {
  int M;
  cin >> M;

  while (M--) {
    int n, m;
    cin >> n;
    cin >> m;
    
    vector< pair<string, int> > dnas;

    // Obtengo los datos
    for (int i = 0; i < m; i++) {
      string genoma;
      cin >> genoma;

      int inverted = 0;
      for (int i = 0; i < n; i++) {
        for(int j = i+1; j < n; j++) {
          if (genoma[i] > genoma[j]) {
            inverted++;
          }
        }
      }
      pair<string, int> newGenoma(genoma, inverted);
      dnas.push_back(newGenoma);
    }

    stable_sort(dnas.begin(), dnas.end(), comp);

    for(vector< pair<string, int> >::iterator it = dnas.begin(); it != dnas.end(); it++) {
      cout << it->first << endl;
    }

    if (M)
      cout << endl;
  }
}
