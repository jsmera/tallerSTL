/* @JUDGE_ID: 10194 C++  "Football (Aka Soccer)" */

/**************
by: Carlos Mera
date: 23/10/2017

**************/
#include <vector>
#include <map>
#include <iostream>
#include <algorithm>
#include <string.h>
#include <stdlib.h>



using namespace std;

struct Team {
  int points = 0;
  int games = 0;
  int wins = 0;
  int ties = 0;
  int losses = 0;
  int goals_d = 0;
  unsigned int goals = 0;
  unsigned int goals_a = 0;
};

bool comp (const pair<string, Team>& x, const pair<string, Team>& y) {
  if (x.second.points != y.second.points) {
    return x.second.points > y.second.points;
  }
  if (x.second.wins != y.second.wins) {
    return x.second.wins > y.second.wins;
  }
  if (x.second.goals_d != y.second.goals_d) {
    return x.second.goals_d > y.second.goals_d;
  }
  if (x.second.goals != y.second.goals) {
    return x.second.goals > y.second.goals;
  }
  if (x.second.games != y.second.games) {
    return x.second.games < y.second.games;
  }

  int res = strcasecmp(x.first.c_str(), y.first.c_str());
  return res < 0; 
}

int main () {
  int N;
  scanf("%d\n", &N);

  while (N--) {
    map<string, Team> tour;
    int T, G;
    char name[101];

    scanf("%[^\n]%*c", name);
    
    scanf("%d\n", &T);
    
    for (int i = 0; i < T; i++) {
      char name_team[31];
      scanf("%[^\n]%*c", name_team);
      Team team;
      tour[name_team] = team;
    }

    scanf("%d\n", &G);
    
    for (int i = 0; i < G; i++) {
      char team1[32], team2[32];
      int g1, g2;

      scanf("%[^#]#%d@%d#%[^\n]\n", team1, &g1, &g2, team2);

      tour[team1].goals += g1;
      tour[team1].goals_a += g2;
      tour[team1].goals_d = tour[team1].goals - tour[team1].goals_a;
      tour[team1].games += 1;
      // Team 2
      tour[team2].goals += g2;
      tour[team2].goals_a += g1;
      tour[team2].goals_d = tour[team2].goals - tour[team2].goals_a;
      tour[team2].games += 1;

      if (g1 > g2) {
        // Win Team 1
        tour[team1].wins++;
        tour[team2].losses++;
        tour[team1].points += 3;
      } else if (g1 < g2) {
        // Win Team 2
        tour[team2].wins++;
        tour[team1].losses++;
        tour[team2].points += 3;
      } else {
        // Ties
        tour[team2].ties++;
        tour[team1].ties++;
        tour[team2].points++;
        tour[team1].points++;
      }
    }

    vector< pair<string, Team> > by_sort;
    for (map<string, Team>::iterator it = tour.begin(); it != tour.end(); it++) {
      by_sort.push_back(*it);
    }

    sort(by_sort.begin(), by_sort.end(), comp);

    printf("%s\n", name);
    int index = 1;
    for (vector< pair<string, Team> >::iterator it = by_sort.begin(); it != by_sort.end(); it++) {
      printf("%d) %s %dp, %dg (%d-%d-%d), %dgd (%d-%d)\n", index, it->first.c_str(), it->second.points,
              it->second.games, it->second.wins, it->second.ties, it->second.losses,
              it->second.goals_d, it->second.goals, it->second.goals_a);
      index++;
    }

    if (N)
      printf("\n");
  }
  return 0;
}
