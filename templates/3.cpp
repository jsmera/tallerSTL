#include <iostream>
#include <vector>
#include <cstdio>
#include <algorithm>
using namespace std;

int main(){
	int n,k;
	cin>>n>>k;
	while (n and k){
		int pagados=0;
		vector<int> vec(n);
		int x=0; 
		int limite=0;
		int coin=1;
		while(pagados < n){
			
			if(vec[x] != 40){
				if(vec[x]+coin >= 40){
					coin-=40 - vec[x];
					vec[x]=40;
					printf("%3d",x+1);
					pagados++;
				}
				else{
					vec[x]+=coin;
					coin=0;
				}
			}
			if (coin == 0) {
				if(limite + 1 >= k){
					limite=0;
				}
				else{
					limite++;
				}
                coin = limite + 1;
            }
            if( x + 1 >= vec.size()){
            	x=0;
			}
			else{
				x++;
			}
		}
		cout<<endl;
		cin>>n>>k;
    }
	return 0;
}
